
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase que implementa un grafo no dirigido por medio de una matriz
 * bidimensional. Ideal para casos de grafos muy densos.
 */
public class Grafo {

    private int m[][]; // la matriz bidimensional que representa al grafo con sus adyacencias
    private int nVertices; // número de vértices del grafo

    /**
     * El constructor crea la matriz de adyacencia para representar al grafo no
     * dirigido
     *
     * @param nVertices número de vertices.
     * @param coor arreglo de coordenadas del punto A al punto B.
     */
    public Grafo(int nVertices, Coordenada[] coor) {

        // Restricción del problema
        if ((1 <= nVertices && nVertices <= 10000 && 0 <= coor.length && coor.length <= (nVertices - 1))) {
            this.nVertices = nVertices;
            m = new int[nVertices][nVertices];

            // Crear las adyacencias
            for (int i = 0; i < coor.length; i++) {
                int a = coor[i].getX();
                int b = coor[i].getY();
                adyacencia(a, b);
            }
        } else {
            System.out.println("Error: No se está cumpliendo con la reestricción del problema:\n"
                    + "n(1 <= n <= 10^4) y m(0 <= m <= n-1)");
        }

    }

    /**
     * Método que crea las adyacencias del vertice A al vertice B.
     *
     * @param a
     * @param b
     */
    public void adyacencia(int a, int b) {

        // Restricción del problema
        if (1 <= a && a <= b && b <= m.length) {
            // por ser un grafo no dirigido, debe tener adyacencia en X y Y y viceversa.
            m[a - 1][b - 1] = 1;
            m[b - 1][a - 1] = 1;
        } else {
            System.out.println("Error en la coordenada a,b(" + a + ", " + b + "). No se está cumpliendo con la reestricción del problema:\n"
                    + "a,b(1 <= a <= b <= n)");
        }

    }

    /**
     * Método que valida si un nodo A es adyacente a un nodo B.
     *
     * @param a
     * @param b
     * @return true si son adyacentes o de lo contrario false.
     */
    public boolean sonAdyacentes(int a, int b) {
        return m[a][b] == 1;
    }

    /**
     * Método que crea los conjuntos que forma inicialmente el grafo
     *
     * @return ArrayList - la lista de conjuntos actuales en el grafo
     */
    public ArrayList<String> conjuntosActuales() {
        StringBuilder cad = new StringBuilder();
        ArrayList<String> conjuntos = new ArrayList<>();
        boolean[] visitados = new boolean[nVertices];
        int i; // fila
        int j; // columna
        for (i = 0; i < m.length; i++) {
            if (!visitados[i]) {
                visitados[i] = true;
                cad.append(i + 1);
                for (j = 0; j < m.length; j++) {
                    if (sonAdyacentes(i, j)) {
                        cad.append(" ").append(j + 1);
                        visitados[j] = true;
                    }
                }
                conjuntos.add(cad.toString());
                cad.delete(0, cad.length()); // reinicia la cadena
            }

        }
        return conjuntos;
    }

    /**
     * Método que invoca a crear los nuevos conjuntos.
     */
    public void conjuntos() {
        nuevosConjuntos(conjuntosActuales());
    }

    /**
     * Método que crea los conjuntos nuevos a partir de los que ya existen en el
     * grafo.
     *
     * @param conjuntosActuales ArrayList de conjuntos actuales en el grafo.
     */
    public void nuevosConjuntos(ArrayList<String> conjuntosActuales) {
        StringBuilder cad = new StringBuilder();
        int contador = 0;
        for (int i = 0; i < conjuntosActuales.size(); i++) {
            for (int j = i + 1; j < conjuntosActuales.size(); j++) {
                contador++;
                cad.append("Salida").append(contador).append(": ").
                        append(conjuntosActuales.get(i)).append(" ").
                        append(conjuntosActuales.get(j)).append("\n");
            }
        }
        cad.append("Total de nuevos conjuntos: ").append(contador);
        System.out.println(cad.toString());

    }

    /**
     * Muestra el grafo representado en una matriz bidimensional
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();

        for (int i = 0; i < nVertices; i++) {
            for (int j = 0; j < nVertices; j++) {
                string.append(" ").append(m[i][j]);
            }
            string.append("\n");
        }
        return string.toString();
    }
}
