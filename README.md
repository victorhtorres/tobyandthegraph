# Toby And The Graph

Ejercicio de la maratón de programación, modificado para entrega de proyecto
final de teoría de la computación.

## Problema

[Descargar pdf](ProblemI_UTPOpenDiv2_2015.pdf)

## Explicación

Para la solución de Toby and the Graph, lo primero que se tiene que considerar, 
es como representar un grafo G=(V, E) internamente. Suponiendo que los vértices 
están enumerados por el valor de entrada que tenga “n”, una forma simple de 
representar un grafo consiste en implementar una matriz bidimensional, 
denominada matriz de adyacencia. Para cada arista (v, w), hacemos que a[v][w] 
sea igual al coste de la arista (si se necesitara con coste) o un valor entero
1 para representar su relación. Los vertices que no tiene relación, pueden inicializarse
con un valor lógico INFINITY o en su defecto, con un valor entero 0, para
representar un coste infinito o dos vertices que no tienen relación alguna:


![Grafo dirigido representado en una matriz de adyacencia.](grafo-matriz.png)


Después, a medida que vamos encontrando aristas, se configura el valor de la 
entrada apropiada. Con esta implementación, la inicialización tarda un tiempo
O(|V|^2). Aunque se puede evitar el coste cuadrático de inicialización, 
el coste en términos de espacio seguirá siendo O(|V|^2), lo cual es adecuado
para grafos densos pero completamente inaceptable para grafos dispersos.

### Formar todos los posibles conjuntos del grafo

Para formar todos los posibles conjuntos de un grafo ya construido, se debe
tener en cuenta que los primeros conjuntos del grafo son las adyacencias que ya
tienen creadas. Por ejemplo:

Dado un grafo G = (V, E), donde V es el conjunto de vértices y E el conjunto de
aristas que representa la relación que tienen dos vertices, donde:

V = {1, 2, 3, 4, 5, 6, 7} 
E = {{2, 3}, {4,5}, {5,6}, {4,6}}

Inicialmente, los conjuntos de este grafo sería:

A={1}, B = {7}, C={2,3}, D={4,5,6}

Gráficamente se vería así:


![Los conjuntos inciales que forma el grafo, dado G=(V, E)](conjuntos-actuales.png)


Para la solución de Toby and the Graph, se debe considerar todas las posibles 
uniones de conjuntos en un grafo dado. Para el ejemplo anterior, sería:

A U B = {1, 7}
A U C = {1, 2, 3}
A U D = {1, 4, 5, 6}
B U C = {2, 3, 7}
B U D = {4, 5, 6, 7}
C U D = {2, 3, 4, 5, 6}

Total de nuevos conjuntos: 6.

### Solución

Para poder encontrar todos los posibles conjuntos del grafo, se debe hacer un 
recorrido por todos los vértices del grafo y sus adyacentes, teniendo en cuenta
cuales ya han sido visitados, para no agregar en la solución conjuntos con 
valores repetidos. La forma más sencilla es primero formar los conjuntos que
forman las aristas del grafo y posterior a eso, concatenar todos los posibles
conjuntos, por medio de una estructura cíclica anidada, donde el ciclo más
externo hace del primero conjunto y el ciclo más interno hace de los conjuntos
siguientes (i = 0, j = i + 1) y de esta forma concatenar todos los posibles
conjuntos del grafo.

### Otros métodos

1. Aún se considera que unir todos los posibles conjuntos tiene un costo muy alto
O(n^2), debido a que la estructura que realiza esto, es un ciclo anidado. Se
busca aún una solución mejor.

2. Otra forma de implementar la estructura de un grafo, es por medio de una lista
de adyacencia, pero este sólo es recomendable para grafos dispersos y no tan
densos como lo son en el ejercicio de Toby and the graph.

